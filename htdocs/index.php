<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Art Store</title>
      <link rel="stylesheet" href="css\styles.css">
	</head>

	<body>
		<header>
         <div id="topHeaderRow">
            <nav>
               <ul>
                  <li><a href="#">My Account</a></li>
                  <li><a href="#">Wish list</a></li>
                  <li><a href="#">Shopping Cart</a></li>
                  <li><a href="#">Checkout</a></li>
               </ul>
            </nav>
         </div>

         <h1>Art Store</h1>

         <div id="mainNavigationRow">
            <a href="index.html">Home</a>
            <a href="about.html">About Us</a>
            <a href="#">Art Works</a>
            <a href="#">Artists</a>
         </div>
      </header>

      <main>
         <h2 id="topMargin">Self-portrait in a Straw Hat</h2>
         <p>By <a href="https://en.wikipedia.org/wiki/%C3%89lisabeth_Vig%C3%A9e_Le_Brun">Louise Elisabeth LeBrun</a><p>

         <figure>
            <img src="images\113010.jpg" alt="Self-Portrait">
            <figcaption>LeBrun's original is recorded in a private collection in france.</figcaption>
         </figure>

         <div id="info">The painting appears, after cleaning, to be an autographed replica of a picture, the original of which was painted in Brussels in 1782 in free imitation of Ruben's 'Chapeau de Paille', which LeBrun has seen in Antwerp. It was exhibited in Paris in 1782 at the Salon de la Correspondance.</div>
         <p class="price">$700</p>

         <form id="buttons" action="#">
            <button type="submit"><a href="#">Add to Wish List</a></button>
            <button type="submit"><a href="#">Add to Shopping Cart</a></button>
         </form>

         <h3>Product Details</h3>
         <table>
            <tr>
               <th>Date:</th>
               <td>1782</td>
            </tr>
            <tr>
               <th>Medium:</th>
               <td>Oil on Canvas</td>
            </tr>
            <tr>
               <th>Dimensions:</th>
               <td>98cm x 71cm</td>
            </tr>
            <tr>
               <th>Home:</th>
               <td><a href="https://en.wikipedia.org/wiki/National_Gallery">National Gallery, London</a></td>
            </tr>
            <tr>
               <th>Genres:</th>
               <td><a href="https://en.wikipedia.org/wiki/Realism_(arts)">Realism</a>, <a href="https://en.wikipedia.org/wiki/Rococo">Rococo</a></td>
            </tr>
            <tr>
               <th>Subjects:</th>
               <td><a href="https://en.wikipedia.org/wiki/People">People</a>, <a href="https://en.wikipedia.org/wiki/The_arts">Arts</a></td>
            </tr>
         </table>

         <h3 id="simProducts">Similar Products</h3>
         <div id="similar"> 
            <div class="box"> 
               <img class="centeredInside" src="images\thumbs\116010.jpg">
               <figcaption class="similarTitle"><a href="https://en.wikipedia.org/wiki/Portrait_of_the_Artist_Holding_a_Thistle">Artist Holding a Thistle</a></figcaption>
            </div>
            <div class="box">
               <img class="centeredInside" src="images\thumbs\120010.jpg">
               <figcaption class="similarTitle"><a href="https://en.wikipedia.org/wiki/Portrait_of_Eleanor_of_Toledo">Portrait of Eleanor of Toledo</a></figcaption>
            </div>
            <div class="box">
               <img class="centeredInside" src="images\thumbs\107010.jpg">
               <figcaption class="similarTitle"><a href="https://en.wikipedia.org/wiki/Madame_de_Pompadour">Madame de Pompadour</a></figcaption>
            </div>
            <div class="box">
               <img class="centeredInside" src="images\thumbs\106020.jpg">
               <figcaption class="similarTitle"><a href="https://en.wikipedia.org/wiki/Girl_with_a_Pearl_Earring">Girl with a Pearl Earring</a></figcaption>
            </div>
         </div>         
		</main>

      <footer>
         All images are copyright to their owners. This is just a hypothetical site &copy 2021 Copyright Art Store
      </footer>
	</body>
</html>