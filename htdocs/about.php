<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Art Store</title>
      <link rel="stylesheet" href="css\styles.css">
	</head>

	<body>
		<header>
         <div id="topHeaderRow">
            <nav>
               <ul>
                  <li><a href="#">My Account</a></li>
                  <li><a href="#">Wish list</a></li>
                  <li><a href="#">Shopping Cart</a></li>
                  <li><a href="#">Checkout</a></li>
               </ul>
            </nav>
         </div>
         
         <h1>Art Store</h1>

         <div id="mainNavigationRow">
         	<a href="index.html">Home</a>
         	<a href="about.php">About Us</a>
         	<a href="#">Art Works</a>
         	<a href="#">Artists</a>
         </div>
      </header>

      <main>
      	<h2 id="topMargin">About Us</h2>
      	<p>This assignment was created by Ethan Border.</p>
      	<p>It was created for CS3140 at Bowling Green State University.</p>
      </main>

      <footer>
         All images are copyright to their owners. This is just a hypothetical site &copy 2021 Copyright Art Store
      </footer>
  </body>